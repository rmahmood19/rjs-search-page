import React,{Component} from 'react';

class SearchBox extends Component {

    render() {
        return (
            <div className="ui search">
            <div className="ui icon input">
            <input  type="text" placeholder="Search a soldier ..."
            onChange={this.props.searchChange}/>
                <i className="search icon"></i>
            </div>
</div>
        );
    }
}
export default SearchBox;