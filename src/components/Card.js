import React, { Component } from 'react';


class Card extends Component {
    render() {
        return (
                <div className="ui centered red card">
                    <div className="content center aligned ">
                    <img className="ui image" alt="robot"
                         src={`https://robohash.org/${this.props.id}.png/?size=150x150`}/>
                    <div className="header">
                        {this.props.name}
                    </div>
                    <p>{this.props.email}</p>
                    </div>
                </div>
        );
    }
}
export default Card;