import React, {Component} from 'react'
import Card from './Card';


class Cardlist extends Component {
    render(){
        const robots = this.props.robots.map((robots) => {
            return(
            <Card 
            key={robots.id}
            id={robots.id}
            name={robots.name}
            email={robots.email}/>
            );
        });
        return (
            <div className="ui cards">
            {robots}
            </div>
        )
    };
};

export default Cardlist;