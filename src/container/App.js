import React from 'react';
import Cardlist from '../components/Cardlist';
import SearchBox from '../components/SearchBox';
import Scroll from '../components/Scroll.js';
import "./App.css";


class App extends React.Component {
  constructor(){
    super()
    this.state = {
      robots :[],
      searchField : ''
    }
  } 
  componentDidMount(){
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(response=>{return response.json();})
    .then(users=>{this.setState({robots:users})})
  }
  onSearchChange = (event) => {
    this.setState({
      searchField : event.target.value,
    })
  }

  render(){
    const filteredRobots = this.state.robots.filter(robots => {
      return robots.name.toLowerCase().includes(this.state.searchField.toLowerCase())
    })
    if (this.state.robots.length === 0) {
      return (
        <h2>LOADING....</h2>
      )
    } else {
      return (
        <div>
          <div className="ui center aligned segment header" ><h2>My RoboSoldiers</h2>
          <div className="ui divider"></div>
          <SearchBox 
          searchChange={this.onSearchChange}/>
          </div>
          <Scroll>        
            <Cardlist robots={filteredRobots}/>
          </Scroll>
        </div>
      );
    }
    
  }
}
export default App ;
